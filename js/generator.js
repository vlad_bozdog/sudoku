export class Generator {
        
    constructor() {}
    
    create(difficulty) {
        
        return new Promise((resolve, reject) => {
            let isNotDificulty = true;
            while ( isNotDificulty ) {
                let sudoku = this.getEmptyBoard(),
                    board = this.generate(sudoku),
                    difficultySteps = this.getDifficultySteps(difficulty);
                
                if ( board.steps >= difficultySteps.min && board.steps <= difficultySteps.max ) {
                    isNotDificulty = false;
                    resolve(this.getBoardArrays(board.board,board.steps, difficulty));
                }
            }
        });
    
    }
    
    getEmptyBoard() {
        const size = 81;
        let board = new Array(size);
            
        board.fill(0,0,size);
        return board;
    }
    
    getDifficultySteps(difficulty) {
        if ( difficulty === 1 ) {
            return {
                min: 80,
                max: 85
            }
        } else if ( difficulty === 2 ) {
            return {
                min: 86,
                max: 90
            }        
        } else {
            return {
                min: 91,
                max: 120
            }      
        }
    }
    
    
    getRow(cell) {
    	return Math.floor( cell / 9 );
    }

    getCol(cell) {
    	return cell % 9;
    }
    
    getBlock(cell) {
    	return Math.floor( this.getRow(cell) / 3) * 3 + Math.floor( this.getCol(cell) / 3) ;
    }
    
    getPossibleValues(cell, sudoku) {
       let possible = new Array();
       for ( let i=1; i<=9; i++ ) {
           if ( this.isPossibleNumber(cell, i, sudoku) ) {
               possible.unshift(i);
           }
       }
       return possible;
    }

    getRandomPossibleValue(possible, cell) {
    	let randomPicked = Math.floor(Math.random() * possible[cell].length);
    	return possible[cell][randomPicked];
    }

    getNextRandom(possible) {
    	let max = 9,
    	    minChoices = 0;
            
    	for ( let i=0; i<=80; i++) {
    		if ( possible[i] != undefined)  {
    			if ( (possible[i].length<=max) && (possible[i].length>0)) {
    				max = possible[i].length;
    				minChoices = i;
    			}
    		}
    	}
    	return minChoices;
    }
                
    isPossibleRow(number, row, sudoku) {
    	for ( let i=0; i<=8; i++) {
    		if (sudoku[row*9+i] == number) {
    			return false;
    		}
    	}
    	return true;
    }
    
    isPossibleCol(number, col, sudoku) {
    	for ( let i=0; i<=8; i++) {
    		if (sudoku[col+9*i] == number) {
    			return false;
    		}
    	}
    	return true;
    }
    
    isPossibleBlock(number, block, sudoku) {
    	for ( let i=0; i<=8; i++ ) {
    		if ( sudoku[ Math.floor(block/3) * 27 + i%3 + 9 * Math.floor(i/3) + 3 * (block%3) ] == number) {
    			return false;
    		}
    	}
    	return true;
    }
    
    isPossibleNumber(cell, number, sudoku) {
    	let row = this.getRow(cell),
    	    col = this.getCol(cell),
    	    block = this.getBlock(cell);
            
    	return this.isPossibleRow(number, row, sudoku) && this.isPossibleCol(number, col, sudoku) && this.isPossibleBlock(number, block, sudoku);
    }
    
    isCorrectRow(row, sudoku) {
    	let rightSequence = new Array(1,2,3,4,5,6,7,8,9),
    	    rowTemp = new Array();
            
    	for ( let i=0; i<=8; i++ ) {
    		rowTemp[i] = sudoku[row*9+i];
    	}
    	rowTemp.sort();
    	return rowTemp.join() == rightSequence.join();
    }
    
    isCorrectCol(col, sudoku) {
    	let rightSequence = new Array(1,2,3,4,5,6,7,8,9),
    	    colTemp = new Array();
            
    	for ( let i=0; i<=8; i++) {
    		colTemp[i] = sudoku[col+i*9];
    	}
    	colTemp.sort();
    	return colTemp.join() == rightSequence.join();
    }
    
    isCorrectBlock(block, sudoku) {
    	let rightSequence = new Array(1,2,3,4,5,6,7,8,9),
    	    blockTemp = new Array();
    	for ( let i=0; i<=8; i++) {
    		blockTemp[i] = sudoku[Math.floor(block/3)*27+i%3+9*Math.floor(i/3)+3*(block%3)];
    	}
    	blockTemp.sort();
    	return blockTemp.join() == rightSequence.join();
    }
    
    isSolvedSudoku(sudoku) {
    	for ( let i=0; i<=8; i++ ) {
    		if ( !this.isCorrectBlock(i, sudoku) || !this.isCorrectRow(i, sudoku) || !this.isCorrectCol(i, sudoku) ) {
    			return false;
    		}
    	}
    	return true;
    }
    
    isSolutionUnique(sudoku) {
    	let possible = new Array();
    	for ( let i=0; i<=80; i++ ) {
    		if ( sudoku[i] == 0 ) {
    			possible[i] = new Array();
    			possible[i] = this.getPossibleValues(i,sudoku);
    			if ( possible[i].length == 0 ) {
    				return false;
    			}
    		}
    	}
    	return possible;
    }
    
    removeAttempt(attemptArray, number) {
    	let newArray = new Array();
    	for ( let i=0; i<attemptArray.length; i++) {
    		if (attemptArray[i] != number) {
    			newArray.unshift(attemptArray[i]);
    		}
    	}
    	return newArray;
    }
    
    generate(sudoku) {
        
        let saved = new Array(),
            savedSudoku = new Array(),
            i = 0,
            nextMove,
            whatToTry,
            attempt;
               
        while ( !this.isSolvedSudoku(sudoku) ) {

            i++;
            nextMove = this.isSolutionUnique(sudoku);

            if ( nextMove == false ) {
                nextMove = saved.pop();
                sudoku = savedSudoku.pop();
            }

            whatToTry = this.getNextRandom(nextMove);
            attempt = this.getRandomPossibleValue(nextMove,whatToTry);

            if ( nextMove[whatToTry].length>1 ) {
                nextMove[whatToTry] = this.removeAttempt(nextMove[whatToTry],attempt);
                saved.push(nextMove.slice());
                savedSudoku.push(sudoku.slice());
            }

            sudoku[whatToTry] = attempt;
        }       
       
        return {
            board: sudoku,
            steps: i
        }
       
    }
    
    getBoardArrays(sudoku, steps, difficulty) {
        
        let solved = [];
        for ( let i=0; i<=8; i++ ) {
            for ( let j=0; j<=8; j++) {
                
                if ( solved[i] === undefined ) {
                    solved[i] = new Array();
                }
                
                solved[i][j] = sudoku[i*9+j];
            }
        }
        
        let masked = [],
            maskedNumber = ( difficulty === 1 ) ? 4 : 5;
        
        for ( let i=0; i<=8; i++ ) {
            let randomHide = Array.from({length: maskedNumber}, () => Math.floor(Math.random() * 8));
            for ( let j=0; j<=8; j++) {
                
                if ( masked[i] === undefined ) {
                    masked[i] = new Array();
                }
                
                if ( randomHide.includes(j) ) {
                    masked[i][j] = '';
                } else {
                    masked[i][j] = solved[i][j];
                }
                
                
            }
        }
        
        return {
            solved,
            masked
        }
    }
}