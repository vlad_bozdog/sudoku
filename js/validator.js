export class Validator {
    
    constructor() {
    }
    
    validate(board) {
        
        const maskedBoardOriginal = board.masked,
              solvedBoardOriginal = board.solved,
              maskedBoard = maskedBoardOriginal.map(function(arr) { return arr.slice(); }),
              maskedBoardUnsorted = maskedBoardOriginal.map(function(arr) { return arr.slice(); }),
              solvedBoard = solvedBoardOriginal.map(function(arr) { return arr.slice(); });
  
        return {
            invalidRows: this.areRowsCorrect(maskedBoard),
            invalidColumns: this.areColumnsCorrect(maskedBoard),
            invalidBlock: this.areBlocksCorrect(maskedBoard),
            isComplete: this.isComplete(maskedBoard, solvedBoard, maskedBoardUnsorted)
        };
    }
    
    areRowsCorrect(data) {
        
        let invalidRows = [],
            index = 0;
        
        for ( let row of data ) {
            
            row = row.filter(function (el) {
                return el != '';
            });
            
            if ( row.length !== new Set(row).size ) {
                invalidRows.push(index);
            }
            
            index++;
        }
        
        return invalidRows;
    }
    
    areColumnsCorrect(data) {
        
        let columnData = [],
            invalidColumns = [],
            index = 0;
        
        for ( let i = 0; i <= 8; i++ ) {
            columnData[i] = new Array();
            for ( let row of data ) {
                columnData[i].push(row[i]);
            }    
        }
        
        for ( let column of columnData ) {
            
            column = column.filter(function (el) {
                return el != '';
            });
            
            if ( column.length !== new Set(column).size ) {
                invalidColumns.push(index);
            }
            
            index++;
        }

        return invalidColumns;
            
    }
    
    areBlocksCorrect(data) {
        
        let blocks = [];
        for ( let i = 0; i <= 6; i = i + 3 ) {
            for ( let j = 0; j <= 6; j = j + 3 ) {
                let blockData = new Array();
                for ( let m = 0; m <= 2; m++ ) {
                    for ( let n = 0; n <= 2; n++ ) {
                        blockData.push(data[i+m][j+n]);
                    }
                }
                blocks.push({
                    data: blockData,
                    origin: [i, j]
                });
            }
        }
        
        let invalidBlocks = [];
            
        for ( let block of blocks ) {
            let blockData = block.data.filter(function (el) {
                return el != '';
            });
            
            if ( blockData.length !== new Set(blockData).size ) {
                invalidBlocks.push(block.origin);
            }
        }
        
        return invalidBlocks;
    }
    
    isComplete(maskedBoard, solvedBoard, maskedBoardUnsorted) {
        
        let rightSequence = new Array(1,2,3,4,5,6,7,8,9).join();
             
        // check rows
        for ( let row of maskedBoard ) {
            if ( row.sort().join() !== rightSequence ) {
                return false;
            }
        }

        // check columns
        let columnData = [],
            index = 0;
        for ( let row of maskedBoard ) {
            columnData[index] = new Array();
            for ( let i = 0; i <= 8; i++ ) {
                columnData[index].push(row[i]);
            }
            index++;
        }
        
        for ( let column of columnData ) {
            if ( column.sort().join() !== rightSequence ) {
                return false;
            }
        }
        
        // check if solutions match
        for ( let i = 0; i <= 8; i++ ) {
            for ( let j = 0; j <= 8; j++ ) {
                if ( solvedBoard[i][j] !== maskedBoardUnsorted[i][j] ) {
                    return false;
                }
            }
        }
        
        return true;
        
    }
}