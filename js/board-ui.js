import { Generator } from './generator.js';
import { Validator } from './validator.js';

export class BoardUi {
    
    constructor() {        
        
        this.validator = new Validator();
        
        this.generator = new Generator();    
        this.generator
            .create(1)
            .then( board => {
                this.board = board;
                this.boardEdited = new Array();
                this.generateBoardHTML(this.board);
                this.generateControlingHTML();
                this.appendEventListeners();
            });
        
        this.editing = {
            x: null,
            y: null
        };
        
        this.difficulty = 1;
        
    }
    
    generateBoardHTML(board) {
        let grid = document.createElement('div'),
            gridHTML = '',
            gameWrapper = document.getElementsByClassName('game-wrapper')[0];

        grid.className = 'game-table';
            
        for ( let i=0; i<=8; i++ ) {
            gridHTML += '<div class="game-row">';
                for ( let j=0; j<=8; j++ ) {
                    gridHTML += '<div class="game-cell">';
                        gridHTML += '<span>' + board.masked[i][j] + '</span>';
                    gridHTML += '</div>';
                }
            gridHTML += '</div>';
        }
            
        grid.innerHTML = gridHTML;
        
        if ( document.getElementsByClassName('game-table').length === 0 ) {
            gameWrapper.appendChild(grid);
        } else {
            document.getElementsByClassName('game-table')[0].parentNode.replaceChild(grid, document.getElementsByClassName('game-table')[0]);
        }
        
        
    }
    
    generateControlingHTML() {
        let controller = document.createElement('div'),
            controllerHTML = '',
            gameWrapper = document.getElementsByClassName('game-wrapper')[0];
    
        controller.className = 'game-controller';
            
        for ( let i=0; i<=8; i++ ) {
            controllerHTML += '<div class="game-controller-button">';
                controllerHTML += i + 1;
            controllerHTML += '</div>';
        }
            
        controller.innerHTML = controllerHTML;
        
        gameWrapper.appendChild(controller);    
    }
    
    appendEventListeners() {
        
        // listen to select the active filling grid element
        this.appendEventListenersGameCell();   
        
        // listen for new game element
        this.appendEventListenersNewGame();
        
        // listen for difficulty changes
        this.appendEventListenersDifficulty();
    
        // listen for difficulty changes
        this.appendEventListenersSelectValue();            
    }
    
    removeEventListeners() {
        // TODO: this does not unbind, find out why
        let gameCells = document.getElementsByClassName('game-cell');
        for ( let gameCell of gameCells ) {
            gameCell.removeEventListener('click', e => this.gameCellClickEvent(e));
        }    
    }
        
    appendEventListenersGameCell() {
        let gameCells = document.getElementsByClassName('game-cell');
        for ( let gameCell of gameCells ) {
            gameCell.addEventListener('click', e => this.gameCellClickEvent(e));
        }    
    }
    
    gameCellClickEvent(e) {
        let el = e.target.parentNode,
            parent = el.parentElement,
            elIndex = [...parent.children].indexOf(el),
            parentIndex = [...parent.parentElement.children].indexOf(parent),
            gameRows = document.getElementsByClassName('game-row');
        
        this.clearActiveSelections(e);
        
        el.classList.add('game-cell-active');
        gameRows[parentIndex].classList.add('game-row-active');
        
        for ( let gameRow of gameRows ) {
            gameRow.children[elIndex].classList.add('game-cell-column-active');
        }
        
        this.editing = {
            x: elIndex,
            y: parentIndex
        };
    }
    
    appendEventListenersNewGame() {
        let newGame = document.getElementsByClassName('game-toolbar-new-game')[0],
            gameLoader = document.getElementsByClassName('game-loader')[0];
            
        newGame.addEventListener('click', () => {
            gameLoader.classList.add('active');
            
            // TODO: find out why the class is removed instantly
            this.generator
                .create(this.difficulty)
                .then( board => {
                    this.board = board;
                    this.boardEdited = new Array();
                    gameLoader.classList.remove('active');
                    this.generateBoardHTML(this.board);
                    this.appendEventListenersGameCell();   
                    this.removeEventListeners();
                });
        });
    }
    
    appendEventListenersDifficulty() {
        let difficultyButtons = document.getElementsByClassName('game-toolbar-difficulty-option');
        for ( let difficultyButton of difficultyButtons ) {
            difficultyButton.addEventListener('click', () => {
                let parent = difficultyButton.parentElement,
                    elIndex = [...parent.children].indexOf(difficultyButton);
                
                for ( let button of difficultyButtons ) {
                    button.classList.remove('option-active');    
                }
                
                difficultyButton.classList.add('option-active');
                this.difficulty = elIndex + 1;
            });
        }
    }
    
    appendEventListenersSelectValue() {
        let gameControllerButtons = document.getElementsByClassName('game-controller-button'),
            gameCells = document.getElementsByClassName('game-cell'),
            gameRows = document.getElementsByClassName('game-row'),
            boardMasked = this.board.masked,
            originalMasked = boardMasked.map(function(arr) { return arr.slice(); });
            
            
        for ( let gameRow of gameRows ) {
            gameRow.classList.remove('game-row-invalid');
        }

        for ( let gameCell of gameCells ) {
            gameCell.classList.remove('game-cell-invalid');
        }
        
        for ( let gameControllerButton of gameControllerButtons ) {
            gameControllerButton.addEventListener('click', (e) => {
                let parent = gameControllerButton.parentElement,
                    elIndex = [...parent.children].indexOf(gameControllerButton),
                    selectedValue = elIndex + 1;
                
                                        
                if ( this.editing.x === null || this.editing.y === null ) {
                    return false;
                }
                
                if ( originalMasked[this.editing.y][this.editing.x] !== '' ) {
                    this.clearActiveSelections(e);
                    return false;
                }

                for ( let gameRow of gameRows ) {
                    gameRow.classList.remove('game-row-invalid');
                }
        
                for ( let gameCell of gameCells ) {
                    gameCell.classList.remove('game-cell-invalid');
                }
                      
                this.boardEdited.push(this.editing.y + ':' + this.editing.x);
                this.board.masked[this.editing.y][this.editing.x] = selectedValue;
                gameRows[this.editing.y].children[this.editing.x].children[0].innerHTML = selectedValue;
                gameRows[this.editing.y].children[this.editing.x].classList.add('game-cell-edited');
                this.clearActiveSelections(e);

                let validate = this.validator.validate(this.board);
                
                if ( validate.invalidRows.length !== 0 ) {
                    for ( let invalidRowIndex of validate.invalidRows ) {
                        gameRows[invalidRowIndex].classList.add('game-row-invalid');
                    }
                }
                
                if ( validate.invalidColumns.length !== 0 ) {
                    for ( let invalidColumnIndex of validate.invalidColumns ) {
                        for ( let gameRow of gameRows ) {
                            gameRow.children[invalidColumnIndex].classList.add('game-cell-invalid');
                        }
                    }
                }
                
                if ( validate.invalidBlock.length !== 0 ) {
                    for ( let invalidBlockIndex of validate.invalidBlock ) {
                        for ( let m = 0; m <= 2; m++ ) {
                            for ( let n = 0; n <= 2; n++ ) {
                                gameRows[m + invalidBlockIndex[0]].children[n + invalidBlockIndex[1]].classList.add('game-cell-invalid');
                            }
                        }
                        
                    }
                }
                
                if ( validate.invalidBlock.length === 0 && validate.invalidBlock.length === 0  && validate.invalidBlock.length === 0  && validate.isComplete === true ) {
                    document.getElementsByClassName('game-table')[0].classList.add('game-complete');
                }
                
            });
        }
    }
            
    clearActiveSelections(e) {
        
        this.editing = {
            x: null,
            y: null
        };
        
        let gameCells = document.getElementsByClassName('game-cell'),
            gameRows = document.getElementsByClassName('game-row');
             
        for ( let gameCell of gameCells ) {
            gameCell.classList.remove('game-cell-active');
            gameCell.classList.remove('game-cell-column-active');
        }
        
        for ( let gameRow of gameRows ) {
            gameRow.classList.remove('game-row-active');
        }
    }
    
}